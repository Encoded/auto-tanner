package scripts.tanner.runnables;

import static scripts.api.ext.Constants.Interface.Parent.TANNING;
import static scripts.api.ext.Constants.Item.COINS;

import org.tribot.api2007.types.RSInterface;
import org.tribot.api2007.types.RSNPC;
import scripts.api.Interact;
import scripts.api.Interfaces;
import scripts.api.Inventory;
import scripts.api.NPCs;
import scripts.api.Player;
import scripts.api.Timing;
import scripts.tanner.data.Vars;

public class TanRunnable implements Runnable {

    @Override
    public String toString() {
        return "Tanning";
    }

    @Override
    public void run() {
        if (!Interfaces.isValid(TANNING)) {
            tradeTanner();
        }
        if (Interfaces.isValid(TANNING)) {
            tanHides();
        }
    }

    private void tradeTanner() {
        RSNPC ellis = NPCs.find().nameEquals("Ellis").getFirst();
        if (Interact.with(ellis).walkTo().cameraTurn().click("Trade")
            && Timing.waitValue(Player::getInteractingCharacter, 1200) != null) {
            Timing.waitCondition(() -> Interfaces.isValid(TANNING), 2100);
        }
    }

    private void tanHides() {
        int coins = Inventory.getCount(COINS);
        int untanned = Inventory.getCount(Vars.get().hide.getUntannedId());
        RSInterface tannerWidget = Interfaces.find().parentEquals(TANNING).childEquals(Vars.get().hide.getChildId()).getFirst();
        if (Interact.with(tannerWidget).click("Tan All")
            && Timing.waitCondition(() -> Inventory.getCount(Vars.get().hide.getUntannedId()) < untanned, 1200)) {
            int tanned = untanned - Inventory.getCount(Vars.get().hide.getUntannedId());
            if (tanned == (coins - Inventory.getCount(COINS)) / Vars.get().hide.getCost()) {
                Vars.get().stats.addCount("tanned", tanned);
            }
        }
    }

}
