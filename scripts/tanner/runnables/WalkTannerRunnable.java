package scripts.tanner.runnables;

import static scripts.api.ext.Constants.Interface.Parent.TANNING;
import static scripts.tanner.data.Constants.CLOSED_DOOR_TILE;
import static scripts.tanner.data.Constants.PATH_TO_DOOR;
import static scripts.tanner.data.Constants.PATH_TO_TANNER;
import static scripts.tanner.data.Constants.TANNER_AREA;

import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSTile;
import scripts.api.ABCUtil;
import scripts.api.Interact;
import scripts.api.Interfaces;
import scripts.api.NPCs;
import scripts.api.Objects;
import scripts.api.PathFinding;
import scripts.api.Player;
import scripts.api.Timing;
import scripts.api.Walking;
import scripts.api.ext.Doors;

public class WalkTannerRunnable implements Runnable {

    private static final RSTile TANNER_TILE = new RSTile(3276, 3191, 0);

    @Override
    public String toString() {
        return "Walking to Tanner";
    }

    @Override
    public void run() {
        if (PathFinding.canReach(TANNER_TILE)) {
            walkToTanner();
        } else if (Player.distanceTo(CLOSED_DOOR_TILE) > 6) {
            walkToDoor();
        } else if (openDoor()) {
            tradeTanner();
        }
    }

    private void walkToTanner() {
        ABCUtil.performRunActivation();
        RSTile[] path = Walking.randomizePath(PATH_TO_TANNER);
        if (Walking.walkPath(path)) {
            Timing.waitCondition(() -> TANNER_AREA.contains(Player.getPosition()), 1200);
        }
    }

    private void walkToDoor() {
        ABCUtil.performRunActivation();
        RSTile[] path = Walking.randomizePath(PATH_TO_DOOR);
        Walking.walkPath(path);
    }

    private boolean openDoor() {
        RSObject door = Objects.find().actionEquals("Open").atPosition(CLOSED_DOOR_TILE).getFirst();
        return Doors.open(door) && Timing.waitCondition(() -> PathFinding.canReach(TANNER_TILE), 1200);
    }

    private void tradeTanner() {
        RSNPC ellis = NPCs.find().nameEquals("Ellis").getFirst();
        if (Interact.with(ellis).walkTo().cameraTurn().click("Trade")
            && Timing.waitValue(Player::getInteractingCharacter, 1200) != null) {
            Timing.waitCondition(() -> Interfaces.isValid(TANNING), 2100);
        }
    }

}
